<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateUserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $id = $this->id ?? '';

        $rules = [
            'name'  => 'required|string|max:255|min:3',
            'email' => "required|email|unique:users,email,{$id},id",
            'password' => 'required|min:8|max:15',
            'image'    => 'nullable|image|max:1024'
        ];

        if($this->method('PUT'))
        {
            $rules['password'] = 'nullable|min:8|max:15';
        }

        return $rules;
    }

    public function messages()
    {
        return [
            'name.required' => 'Campo Nome é obrigatório!',
            'name.string'   => 'Campo Nome deve ser uma string!',
            'name.min' => 'Campo Nome deve conter mais de 3 cracteres',
            'name.max' => 'Campo Nome deve conter menos de 255 cracteres',
            'email.required' => 'Campo E-mail é obrigatório!',
            'email.email' => 'Inform um e-mail válido!',
            'email.unique' => 'O E-mail informado já está em uso!',
            'password.required' => 'Campo Senha é obrigatório!',
            'password.min' => 'Campo Senha deve conter no mínimo 8 cracteres',
            'password.max' => 'Campo Senha deve conter no máximo 15 cracteres',
            'image.image'  => 'O campo imagem deve conter uma imagem válida!',
            'image.max'    => 'A imagem não pode ultrapassar 1mb!'
        ];
    }
}
