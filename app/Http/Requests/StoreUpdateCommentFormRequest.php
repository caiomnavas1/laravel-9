<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUpdateCommentFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'content' => 'required|min:3|max:10000'
        ];
    }

    public function messages()
    {
        return [
            'content.required' => 'A Mensagem é campo obrigatório!',
            'content.min'      => 'A Mensagem deve conter no mínimo 3 caracteres.',
            'content.max'      => 'A Mensagem deve conter no máximo 10.000 caracteres.'
        ];
    }
}
