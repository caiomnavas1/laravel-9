@extends('layouts.app')
@section('title', "Editar o comentário!")
@section('content')
    <h1 class="text-2xl font-semibold leading-tigh py-2">Editar o comentário!</h1>
    @include('includes.validationsErrors')
    <form action="{{ route('comments.update', $comment->id) }}" method="post">
        @method('PUT')
        @include('users.comments._partials.form')
    </form>
@endsection
